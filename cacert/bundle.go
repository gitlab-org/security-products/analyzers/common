package cacert

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
)

// bundle combines a CA bundle with the path where it is imported.
type bundle struct {
	content string // content is the CA bundle to import.
	path    string // path is where the CA bundle is imported.
}

// isEmpty returns true if the import path or bundle content is empty
func (b bundle) isEmpty() bool {
	return strings.TrimSpace(b.content) == "" || b.path == ""
}

// write writes the CA bundle to its import path.
func (b bundle) write() error {
	if b.isEmpty() {
		return errors.New("cert bundle write: empty content/path")
	}

	log.Debugf("importing custom CA cert bundle to: %q", b.path)

	// create parent directory for the import path
	if err := os.MkdirAll(filepath.Dir(b.path), 0755); err != nil {
		return err
	}

	// If the file doesn't exist, create it, or append to the file
	f, err := os.OpenFile(b.path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = fmt.Fprintln(f, b.content)
	return err
}
