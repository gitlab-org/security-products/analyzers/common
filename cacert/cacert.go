// Package cacert provides code to import CA certificate bundles.
// It is configured by CLI flags and environment variables.
package cacert

import (
	"os"
	"os/exec"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

const (
	// EnvVarBundleContent is the env var for the CA bundle to import
	EnvVarBundleContent = "ADDITIONAL_CA_CERT_BUNDLE"

	// FlagBundleContent is the CLI flag for the CA bundle to import
	FlagBundleContent = "additional-ca-cert-bundle"

	// DefaultBundlePath is the default import path of the CA bundle
	DefaultBundlePath = "/etc/ssl/certs/ca-certificates.crt"

	// DefaultUBIBundlePath is the default import path of the CA bundle for UBI based images
	DefaultUBIBundlePath = "/etc/pki/ca-trust/source/anchors/ca-certificates.crt"

	// release file's path in RHEL images
	redhatReleaseFilePath = "/etc/redhat-release"

	// command to update custom certificates in the system-wide trust store
	// in UBI-based images
	ubiUpdateTrustCmd = "update-ca-trust"
)

// NewFlags returns CLI flags to configure the import of a CA bundle.
func NewFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    FlagBundleContent,
			Usage:   "Additional CA certs bundle to import",
			EnvVars: []string{EnvVarBundleContent},
		},
	}
}

// ImportOptions provides a way to specify optional arguments for the Import function.
type ImportOptions struct {
	Path string // Path specifies where to write the additional CA cert bundle.
}

// Import writes the CA bundle to its import path.
// This is a no-op if the CA bundle is empty.
func Import(c *cli.Context, opts ImportOptions) error {
	path := opts.Path
	if path == "" {
		path = defaultBundlePath()
	}

	b := bundle{
		content: c.String(FlagBundleContent),
		path:    path,
	}

	// skip if bundle or import path is empty
	if b.isEmpty() {
		log.Debugf("CA cert bundle not imported: empty bundle or empty target path")
		return nil
	}

	if err := b.write(); err != nil {
		return err
	}

	if IsUBIImage() {
		return updateUBICerts()
	}

	return nil
}

// IsUBIImage returns true if the current OS is a RHEL-based image
func IsUBIImage() bool {
	_, err := os.Stat(redhatReleaseFilePath)
	return err == nil
}

func defaultBundlePath() string {
	if IsUBIImage() {
		return DefaultUBIBundlePath
	}
	return DefaultBundlePath
}

// updateUBICerts runs `update-ca-trust` CLI command to extract the custom certs added in the
// trust anchors (/etc/pki/ca-trust/source/anchors or /usr/share/pki/ca-trust-source/anchors/)
// into system-wide trust store.
//
// reference: https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/sec-shared-system-certificates
func updateUBICerts() error {
	cmd := exec.Command(ubiUpdateTrustCmd) // #nosec G204
	cmd.Env = os.Environ()

	output, err := cmd.CombinedOutput()
	if err != nil {
		log.Errorf("failed to update custom certificate - error: %s, output: %s", err, output)
		return err
	}
	log.Debugln("custom certificate updated in the CA bundle")

	return nil
}
