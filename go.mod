module gitlab.com/gitlab-org/security-products/analyzers/common/v3

require (
	github.com/bmatcuk/doublestar v1.3.4
	github.com/kr/text v0.2.0 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/sirupsen/logrus v1.9.3
	github.com/stretchr/testify v1.8.0
	github.com/urfave/cli/v2 v2.27.2
	github.com/xrash/smetrics v0.0.0-20240521201337-686a1a2994c1 // indirect
	golang.org/x/sys v0.22.0 // indirect
	gopkg.in/check.v1 v1.0.0-20200902074654-038fdea0a05b // indirect
)

go 1.13
